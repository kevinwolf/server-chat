import express from 'express';
import { Server } from 'http';
import sio from 'socket.io';
import {port} from '../package.json';

// Server variables.
const app = express();
const server = new Server(app);
const io = sio(server);
const socketPort = process.env.PORT || port;

// Members list.
let members = {};

// New connection handler.
io.on('connection', socket => {

  // When a new Dealer connects.
  socket.on('add dealer', (dealer) => {
    members[socket.id] = { name : dealer.name, room : dealer.room, type : 'dealer' };
    socket.join(dealer.room);
    io.to(dealer.room).emit('new dealer', dealer.name);
  });

  // When a new Player connects.
  socket.on('add player', (player) => {
    members[socket.id] = { name : player.name, room : player.room, type : 'player' };
    socket.join(player.room);
    io.to(player.room).emit('new player', player.name);
  });

  // When a new message has been sent.
  socket.on('add message', (message) => {
    socket.broadcast.to(members[socket.id].room).emit('new message', { player : members[socket.id].name, message : message });
  });

  // When a player leaves the table.
  socket.on('disconnect', () => {
    socket.broadcast.to(members[socket.id].room).emit('player leaved', members[socket.id].name);
    delete members[socket.id];
  });

});

// Display a welcome message on the server.
app.get('/', (req, res) => {
  res.send('<h1>Apogee Live Chat Server</h1><h2>Version 0.0.1</h2>');
});

// Start server.
server.listen(socketPort, () => console.log(`Chat server listening on port: ${socketPort}`));

// Export the server object.
export default server;
