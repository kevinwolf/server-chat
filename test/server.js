import should from 'should';
import io from 'socket.io-client';
import request from 'supertest';
import server from '../server/server';
import {port} from '../package.json';

const SOCKET_URL = `http://0.0.0.0:${port}`;
const options = {
  transports             : [ 'websocket' ],
  'force new connection' : true
};

function addDealer (room, name) {
  return new Promise((resolve) => {
    const client = io.connect(SOCKET_URL, options);

    client.once('connect', () => {
      client.emit('add dealer', { room : room, name : name });

      client.once('new dealer', (dealer) => {
        resolve({ name : dealer, socket : client });
      });
    });
  });
}

function addPlayer (room, name) {
  return new Promise((resolve) => {
    const client = io.connect(SOCKET_URL, options);

    client.once('connect', () => {
      client.emit('add player', { room : room, name : name });

      client.once('new player', (player) => {
        resolve({ name : player, socket : client });
      });
    });
  });
}

function halfSecond () {
  return new Promise((resolve) => {
    setTimeout(resolve, 500);
  });
}

describe('The Chat server', () => {

  it('Should return a welcome message on the root page', (done) => {
    request(server)
      .get('/')
      .expect(200, done);
  });

  it('Should notify the room when a new player joins the table', async () => {

    function playerAdded (client) {
      return new Promise((resolve) => {
        client.once('new player', (player) => {
          resolve(player);
        });
      });
    }

    const members = [
      await addDealer('Test room', 'Test Dealer'),
      await addPlayer('Test room', 'Test Player 1'),
      await addPlayer('Test room', 'Test Player 2')
    ];

    await halfSecond();
    members[0].socket.emit('add player', { room : 'Test room', name : 'New Player' });
    let results = await *members.map((member) => playerAdded(member.socket));
    results.map((result) => result.should.equal('New Player'));

    members[0].socket.disconnect();
    members[1].socket.disconnect();
    members[2].socket.disconnect();
  });

  it('Should notify the room when a player leaves the table', async () => {

    function playerLeaved (client) {
      return new Promise((resolve) => {
        client.once('player leaved', (player) => {
          resolve(player);
        });
      });
    }

    const members = [
      await addDealer('Test room', 'Test Dealer'),
      await addPlayer('Test room', 'Test Player 1'),
      await addPlayer('Test room', 'Test Player 2'),
      await addPlayer('Test room', 'Test Player 3'),
      await addPlayer('Test room', 'Test Player 4'),
      await addPlayer('Test room', 'Test Player 5')
    ];

    members[3].socket.disconnect();
    members.splice(3, 1);
    let results = await *members.map((member) => playerLeaved(member.socket));
    results.map((result) => result.should.equal('Test Player 3'));

    members[0].socket.disconnect();
    members[1].socket.disconnect();
    members[2].socket.disconnect();
    members[3].socket.disconnect();
    members[4].socket.disconnect();
  });

  it('Should notify the room when the dealer has changed', async () => {

    function dealerChanged (client) {
      return new Promise((resolve) => {
        client.on('new dealer', (dealer) => {
          resolve(dealer);
        });
      });
    }

    const members = [
      await addDealer('Test room', 'Test Dealer'),
      await addPlayer('Test room', 'Test Player 1'),
      await addPlayer('Test room', 'Test Player 2')
    ];

    members[0].socket.emit('add dealer', { room : 'Test room', name : 'New Dealer' });
    members[0].socket.disconnect();
    members.splice(0, 1);

    let results = await *members.map((member) => dealerChanged(member.socket));
    results.map((result) => result.should.equal('New Dealer'));

    members[0].socket.disconnect();
    members[1].socket.disconnect();
  });

  it('Should notify the room when a new message has been sent', async () => {

    function messageReceived (client) {
      return new Promise((resolve) => {
        client.on('new message', (message) => {
          resolve(message);
        });
      });
    }

    const members = [
      await addDealer('Test room', 'Test Dealer'),
      await addPlayer('Test room', 'Test Player 1'),
      await addPlayer('Test room', 'Test Player 2')
    ];

    members[1].socket.emit('add message', 'Hello players!');
    members[1].socket.disconnect();
    members.splice(1, 1);

    let results = await *members.map((member) => messageReceived(member.socket));
    results.map((result) => {
      result.player.should.equal('Test Player 1');
      result.message.should.equal('Hello players!');
    });

    members[0].socket.disconnect();
    members[1].socket.disconnect();
  });

});
