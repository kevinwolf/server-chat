#Apogee Chat Server
This is the server that will emit the messages to all the chat clients.

##Developing
1. Everytime you pull the features from this repository, you should run `npm install` in order to install all new packages.

2. Create a new feature branch or pull an existing one. Name it according to `git-flow` conventions. For example: `feature/new-feature-name`.

3. Work on TDD mode, and make sure the `eslint` validator approves the code and all the tests passes, generating 100% coverage. **Otherwise, when you merge your code back to develop, the build will fail**.

4. Push your feature branch to Bitbucket and **create a pull request**. Add other developers as a reviewers and **wait them to approve the changes**.

5. Once the developers approved the changes, merge the feature branch with `develop`.

##Deploying
Deploys happens automatically when merging the code to `develop` or `master`, only if the build passes on Apogee Codeship.

###Staging
http://10.28.10.85:1101/

###Production
http://10.28.10.95:1101/
